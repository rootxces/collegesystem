package com.mark42.collegeSystem.database;

import java.util.HashMap;
import java.util.Map;

import com.mark42.collegeSystem.model.Message;
import com.mark42.collegeSystem.model.Profile;

public class DatabaseClass {
	
	private static Map<Long, Message> messages = new HashMap<>();
	private static Map<Long, Profile> profiles = new HashMap<>();
	
	public static Map<Long, Profile> getProfiles(){
		return profiles;
	}
	
	public static Map<Long, Message> getMessages(){
		return messages;
	}
}
