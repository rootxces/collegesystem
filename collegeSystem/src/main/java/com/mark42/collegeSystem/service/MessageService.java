package com.mark42.collegeSystem.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mark42.collegeSystem.database.DatabaseClass;
import com.mark42.collegeSystem.model.Message;
import com.mark42.collegeSystem.model.Profile;

public class MessageService {
	
	public MessageService() {
		messages.put(1L, new Message(1,"Hey there!", "kapil"));
		messages.put(2L, new Message(2,"How are you doing?", "kapil"));
		messages.put(3L, new Message(3,"I am good. Thank you.", "kapil"));
	}
	
	private Map<Long, Message> messages = DatabaseClass.getMessages();
	
	//Returns all the messages on the server
	public List<Message> getAllMessages(){
		return new ArrayList<Message>(messages.values());
	}
	
	//Returns a single message with the given id
	public Message getMessage(long id) {
		return messages.get(id);
	}
	
	//Add a message to the messages
	public Message addMessage(Message message) {
		message.setId(messages.size()+1);
		messages.put(message.getId(),message);
		return message;
	}
	
	//Update a message in messages
	public Message updateMessage(Message message) {
		if(message.getId()<=0) {
			return null;
		}
		messages.put(message.getId(), message);
		return message;
	}
	
	//Remove message in messages
	public Message removeMessage(long id) {
		return messages.remove(id);
	}

}
